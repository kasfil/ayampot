<?php
return [
    'nextActive' => '<a class="pagination-next" rel="next" href="{{url}}">{{text}}</a>',
    'nextDisabled' => '<a class="pagination-next" rel="next" disabled>{{text}}</a>',
    'prevActive' => '<a class="pagination-previous" rel="prev" href="{{url}}">{{text}}</a>',
    'prevDisabled' => '<a class="pagination-previous" rel="prev" disabled>{{text}}</a>',
    'counterRange' => '{{start}} - {{end}} of {{count}}',
    'counterPages' => 'Halaman {{page}} dari {{pages}}',
    'first' => '<li><a class="pagination-link" href="{{url}}">{{text}}</a></li>',
    'last' => '<li><a class="pagination-link" href="{{url}}">{{text}}</a></li>',
    'number' => '<li><a class="pagination-link" href="{{url}}">{{text}}</a></li>',
    'current' => '<li><a class="pagination-link is-current" href="">{{text}}</a></li>',
    'ellipsis' => '<li><span class="pagination-ellipsis">&hellip;</span></li>',
];
