<?php $this->append('css'); ?>
  <?= $this->Html->css('client/main'); ?>
  <?= $this->Html->css('component/notification'); ?>
<?php $this->end(); ?>

<?php $this->append('endScript'); ?>
  <?= $this->Html->script('element/bulmaflash'); ?>
  <?= $this->Html->script('prizeParser'); ?>
<?php $this->end(); ?>

<?php $this->append('js'); ?>
  <?= $this->Html->script('element/bulmaflash'); ?>
<?php $this->end(); ?>

<?= $this->element('client/navbar'); ?>
<?= $this->Flash->render(); ?>
<div class="hero is-medium is-success">
  <!-- Hero Body -->
  <div class="hero-body" id="highlight">
    <div class="container has-text-centered" id="title">
      <h1 class="title is-size-4-touch is-size-2-desktop">Sentral Ayam</h1>
      <h2 class="subtitle is-size-6-touch is-size-5-desktop">Ayam Segar siap antar sampai tempat</h2>
    </div>
  </div>
</div>
<div class="logo">
  <div class="container">
    <div class="big-logo-wrapper">
      <div class="big-logo-bg">
        <img class="logo-center" src="<?= $this->Url->image('logo/logo-dark-1v1.png') ?>" alt="">
      </div>
    </div>
    <div class="after-logo has-text-centered">
      <p class="title has-text-grey-darker is-size-4-desktop is-size-5-touch">Pesanan anda akan kami antar sampai tujuan</p>
      <a class="button has-text-weight-bold is-danger" href="/pesanan">Pesan Sekarang</a>
      <br>
      <a class="subtitle has-text-grey-darker is-size-5">atau chat kami melalui WhatsApp</a>
    </div>
  </div>
</div>
<section class="section has-background-primary">
  <div class="container">
    <h2 class="is-size-3 is-size-4-touch has-text-dark has-text-weight-bold has-text-centered" style="margin-bottom: 25px;">
      Harga untuk hari ini
    </h2>
    <div class="level is-mobile">
      <div class="level-item has-text-centered">
        <div>
          <p class="heading is-size-6">Harga per Kg</p>
          <p class="title" id="KgTag"></p>
        </div>
      </div>
      <div class="level-item has-text-centered">
        <div>
          <p class="heading is-size-6">Harga per Ekor</p>
          <p class="title" id="EkorTag"></p>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="services">
  <div class="container">
    <div class="columns">
      <div class="column">
        <img class="card-image" src="<?= $this->Url->image('fresh-min.png') ?>" alt="Fresh">
        <div class="has-text-centered has-text-dark has-text-weight-bold is-flex title-wrap">
          <p class="is-size-3-desktop is-size-4-touch">Ayam Segar</p>
        </div>
        <p class="subtitle is-size-6-touch has-text-centered has-text-dark">
          Ayam akan kami proses setelah pesanan anda valid untuk memastikan pesanan anda selalu segar dan tidak membusuk
        </p>
      </div>
      <div class="column">
        <img class="card-image" src="<?= $this->Url->image('delivery-min.png') ?>" alt="Fresh">
        <div class="has-text-centered has-text-dark has-text-weight-bold is-flex title-wrap">
          <p class="is-size-3-desktop is-size-4-touch">Antar Sampai Tujuan</p>
        </div>
        <p class="subtitle is-size-6-touch has-text-centered has-text-dark">
          Ayam akan kami proses setelah pesanan anda valid untuk memastikan pesanan anda selalu segar dan tidak membusuk
        </p>
      </div>
      <div class="column">
        <img class="card-image" src="<?= $this->Url->image('healty-min.png') ?>" alt="Fresh">
        <div class="has-text-centered has-text-dark has-text-weight-bold is-flex title-wrap">
          <p class="is-size-3-desktop is-size-4-touch">Ayam Sehat Terjamin</p>
        </div>
        <p class="subtitle is-size-6-touch has-text-centered has-text-dark">
          Ayam akan kami proses setelah pesanan anda valid untuk memastikan pesanan anda selalu segar dan tidak membusuk
        </p>
      </div>
    </div>
  </div>
</div>
<div class="visi has-background-dark">
  <div class="container">
    <div class="columns title is-centered">
      <div class="column is-5">
        <p class="title is-size-3-desktop is-size-4-touch has-text-light has-text-centered">Kami Selalu Memberikan Pelayanan Terbaik</p>
      </div>
    </div>
    <div class="columns desc is-centered">
      <div class="column is-8">
        <p class="subtitle is-size-5-desktop is-size-6-touch has-text-light has-text-centered">
          Karena dalam setiap kepuasan anda akan memberikan kami semangat untuk
          selalu mengembangkan pelayanan untuk anda dari waktu ke waktu.
        </p>
      </div>
    </div>
  </div>
</div>
<?= $this->element('client/footer'); ?>
<script>
  const kgPrize = <?= $prize['kg'] ?>;
  const ekorPrize = <?= $prize['ekor'] ?>;
</script>
