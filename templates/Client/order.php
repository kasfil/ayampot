<!-- Load Additional css -->
<?php $this->append('css'); ?>
  <?= $this->Html->css('component/notification'); ?>
<?php $this->end(); ?>

<!-- Adding additional javascript -->
<?php $this->append('js'); ?>
  <?= $this->Html->script('element/bulmaflash'); ?>
<?php $this->end(); ?>

<!-- Code that will be rendered -->
<?= $this->element('client/navbar'); ?>
<?= $this->Flash->render(); ?>
<section class="hero is-warning is-bold">
  <div class="hero-body has-text-centered">
    <div class="container">
      <p class="title has-text-dark is-size-3-destop is-size-4-touch">
        Selamat Datang
      </p>
      <p class="subtitle has-text-dark is-size-5-desktop is-size-6-touch">
        Pemesanan Online Sentral Ayam
      </p>
    </div>
  </div>
</section>
<section class="section form has-background-lighter">
  <div class="container">
    <div class="columns is-centered">
      <div class="column is-8">
        <?= $this->Form->create($pesanan, ['url' => ['action' => 'order']]); ?>
          <div class="field is-horizontal">
            <div class="field-label is-normal">
              <label class="label" for="nama">Nama</label>
            </div>
            <div class="field-body">
              <div class="field">
                <p class="control is-expanded">
                  <input class="input" id="name" type="text" name="nama">
                </p>
              </div>
            </div>
          </div>
          <div class="field is-horizontal">
            <div class="field-label is-normal">
              <label class="label" for="alamat">Alamat</label>
            </div>
            <div class="field-body">
              <div class="field">
                <div class="control is-expanded">
                  <textarea id="alamat" class="textarea" name="alamat"></textarea>
                </div>
                <p class="help">Mohon diisi selengkap dan sejelas mungkin</p>
              </div>
            </div>
          </div>
          <div class="field is-horizontal">
            <div class="field-label is-normal">
              <label class="label" for="phone">No Handphone</label>
            </div>
            <div class="field-body">
              <div class="field is-expanded">
                <div class="field has-addons">
                  <p class="control">
                    <a class="button is-static">+62</a>
                  </p>
                  <p class="control is-expanded">
                    <input class="input" id="phone" type="tel" name="phone">
                  </p>
                </div>
                <p class="help">Hilangkan 0 didepan</p>
              </div>
            </div>
          </div>
          <div class="field is-horizontal">
            <div class="field-label is-normal">
              <label class="label" for="amount">Jumlah Pesanan</label>
            </div>
            <div class="field-body">
              <div class="field is-expanded">
                <div class="field has-addons">
                  <p class="control is-expanded">
                    <input id="amount" class="input" type="number" name="amount" step="0.1" min="1" value="1.0">
                  </p>
                  <p class="control">
                    <span class="select">
                      <select id="amount_type" name="amount_type">
                        <option value="Kg">Kg</option>
                        <option value="Ekor">Ekor</option>
                      </select>
                    </span>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="field is-horizontal">
            <div class="field-label is-normal">
              <label class="label" for="notes">Catatan</label>
            </div>
            <div class="field-body">
              <div class="field">
                <div class="control is-expanded">
                  <textarea id="notes" class="textarea" name="notes"></textarea>
                </div>
                <p class="help">Catatan untuk kami (optional)</p>
              </div>
            </div>
          </div>
          <div class="field is-horizontal">
            <div class="field-label">
              <!-- Left empty for spacing -->
            </div>
            <div class="field-body">
              <div class="field is-grouped">
                <div class="control">
                  <input class="button is-primary" type="submit" value="Pesan Sekarang">
                </div>
                <div class="control">
                  <a class="button is-light" href="/">Batal</a>
                </div>
              </div>
            </div>
          </div>
        <?= $this->Form->end(); ?>
      </div>
    </div>
  </div>
</section>
<?= $this->element('client/footer'); ?>
