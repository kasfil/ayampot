<!DOCTYPE html>
<html lang="en">
  <head>
    <?= $this->Html->charset(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge;" />
    <?= $this->fetch('meta') ?>
    <?= $this->Html->meta('favicon.ico', '/favicon.png', ['type' => 'icon']); ?>

    <title><?= $this->fetch('title') ?></title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.8.1/css/bulma.min.css" integrity="sha256-1nEaE91OpXJD7M6W5uSiqxhdmrY+lOOTHC1iUxU9Pds=" crossorigin="anonymous" />
    <?= $this->Html->script('fontawesome/js/all.min.js'); ?>
    <script src="https://cdn.jsdelivr.net/npm/umbrellajs@3.1.0/umbrella.min.js" integrity="sha256-jwMuAKE6AAulqnnJ+Bq/nwLQfX4lMQ8Ojd1O+w+1KI4=" crossorigin="anonymous"></script>

    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
  </head>
  <body>
    <?= $this->fetch('content') ?>
    <?= $this->fetch('endScript') ?>
  </body>
</html>
