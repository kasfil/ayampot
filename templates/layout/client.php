<!DOCTYPE html>
<html>
  <head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?= $meta['description'] ?>">
    <meta name="keywords" content="<?= $meta['keywords'] ?>">
    <meta name="robots" content="<?= $meta['robots'] ?>">
    <meta name="revisit-after" content="<?= $meta['revisit'] ?> days">
    <link rel="canonical" href="https://<?= $meta['canonical'] ?>">
    <title>
      <?= $title ?>
    </title>
    <?= $this->Html->meta('favicon.ico', '/favicon.png', ['type' => 'icon']); ?>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
    <script src="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.12.1/js/all.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/umbrellajs@3.1.0/umbrella.min.js"></script>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
  </head>
  <body>
    <?= $this->fetch('content') ?>
    <?= $this->fetch('endScript') ?>
    <?php if($meta['analytic_key'] != ''): ?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?= $meta['analytic_key'] ?>"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', <?= $meta['analytic_key'] ?>);
    </script>
    <?php endif; ?>
  </body>
</html>
