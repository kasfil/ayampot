<?php $this->append('css'); ?>
<?= $this->Html->css('component/notification'); ?>
<?= $this->Html->css('guard/users'); ?>
<?php $this->end(); ?>

<?php $this->append('endScript'); ?>
<?= $this->Html->script('modal-toggle'); ?>
<?= $this->Html->script('element/bulmaflash'); ?>
<?php $this->end(); ?>

<?= $this->element('guard/navbar') ?>
<?= $this->Flash->render(); ?>
<section class="section is-medium has-background-light">
  <div class="container">
    <div class="columns is-centered has-background-grey-lighter">
      <div class="column is-6">
        <div class="level">
          <div class="level-left">
            <div class="level-item">
              <p class="has-text-weight-bold is-size-4">Update password</p>
            </div>
          </div>
          <div class="level-right">
            <div class="level-item">
              <?= $this->Form->create(null, ['url' => ['controller' => 'Users', 'action' => 'updatePassword', 'prefix' => 'Guard']]); ?>
              <div class="field has-addons">
                <div class="control">
                  <input id="password" class="input" type="text" name="password" placeholder="Ketik password baru" required>
                </div>
                <div class="control">
                  <input class="button is-success" type="submit" value="Update">
                </div>
              </div>
              <?= $this->Form->end(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <div class="columns">
      <div class="column is-6">
        <p class="title is-size-3">Daftar user</p>
        <div class="table-container">
          <table class="table is-striped is-fullwidth">
            <thead class="has-background-info">
              <tr>
                <th>Username</th>
                <th class="has-text-centered">active?</th>
                <th class="has-text-centered absorbing-column">action</th>
              </tr>
            </thead>
            <tbody>
              <?php if (count($users) === 0): ?>
              <tr>
                <td colspan="3">User tidak ditemukan</td>
              </tr>
              <?php else: ?>
              <?php foreach ($users as $user): ?>
              <?php $active = '<p class="has-text-success">Yes</p>'; ?>
              <?php $inactive = '<p class="has-text-danger">No</p>'; ?>
              <tr>
                <td><?= $user->username; ?></td>
                <td class="has-text-centered"><?= $user->is_active ? $active : $inactive; ?></td>
                <td class="action" align="right">
                  <?= $this->Html->link(($user->is_active ? 'Nonaktifkan' : 'aktifkan'), ["controller" => "Users", "action" => "toggle", $user->username], ["class" => "button is-warning"]); ?>
                  <a class="button is-danger" id="show-modal" data-target="uid_<?= $user->id ?>">Hapus</a>
                </td>
              </tr>
              <?php endforeach; ?>
              <?php endif; ?>
            </tbody>
          </table>
          <?php if (count($users) !== 0): ?>
          <?php foreach ($users as $user): ?>
          <div id="uid_<?= $user->id ?>" class="modal">
            <div class="modal-background"></div>
            <div class="modal-card">
              <div class="modal-card-head">
                <p class="modal-card-title">Konfirmasi Penghapusan</p>
              </div>
              <section class="modal-card-body has-text-centered">
                <p class="subtitle is-size-4">
                  Anda yakin untuk menghapus user <br>
                  <?= $user->username; ?>
                </p>
              </section>
              <div class="modal-card-foot is-grouped-right">
                <?= $this->Html->link('Hapus', ["controller" => "Users", "action" => "delete", $user->username], ["class" => "button is-danger"]); ?>
                <a class="button is-info" id="close-modal" data-target="uid_<?= $user->id ?>">Batal</a>
              </div>
            </div>
          </div>
          <?php endforeach; ?>
          <?php endif; ?>
        </div>
      </div>
      <div class="column is-6">
        <p class="title is-size-3">Tambah user</p>
        <?= $this->Form->create($user, ['url' => ['controller' => 'Users', 'action' => 'load', 'prefix' => 'Guard'], 'type' => 'POST']); ?>
        <div class="field is-horizontal">
          <div class="field-label">
            <label class="label" for="username">Username</label>
          </div>
          <div class="field-body">
            <div class="field">
              <div class="control is-expanded">
                <input id="username" class="input" type="text" name="username">
              </div>
              <?php if (isset($errors['username'])): ?>
              <p class="help is-danger"><?= $errors['username'][array_key_first($errors['username'])]; ?></p>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <div class="field is-horizontal">
          <div class="field-label">
            <label class="label" for="password">Password</label>
          </div>
          <div class="field-body">
            <div class="field">
              <div class="control is-expanded">
                <input id="password" class="input" type="password" name="password">
              </div>
              <?php if (isset($errors['password'])): ?>
              <p class="help is-danger"><?= $errors['password'][array_key_first($errors['password'])]; ?></p>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <div class="field is-horizontal">
          <div class="field-label">
            <label class="label" for="email">Email</label>
          </div>
          <div class="field-body">
            <div class="field">
              <div class="control is-expanded">
                <input id="email" class="input" type="text" name="email">
              </div>
              <?php if (isset($errors['email'])): ?>
              <p class="help is-danger"><?= $errors['email'][array_key_first($errors['email'])]; ?></p>
              <?php else: ?>
              <p class="help">Bisa Dikosongi (Optional)</p>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <div class="field is-horizontal">
          <div class="field-label">
          </div>
          <div class="field-body">
            <div class="field">
              <div class="control">
                <input class="button is-primary" type="submit" value="Tambah User">
              </div>
            </div>
          </div>
        </div>
        <?= $this->Form->end(); ?>
      </div>
    </div>
  </div>
</section>
<?= $this->element('guard/footer') ?>
