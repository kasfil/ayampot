<?php
function tabs($value, $query, $status) {
  $link = '/guard/pesanan';

  if (strtolower($value) != $status && $query != null) {
    $link = $link . '?status='. strtolower($value) . '&query=' . $query;
    $tag = '<li><a href="'. $link .'">'. $value .'</a></li>';
  } elseif (strtolower($value) != $status && $query == null) {
    $link = $link . '?status='. strtolower($value);
    $tag = '<li><a href="'. $link .'">'. $value .'</a></li>';
  } else {
    $tag = '<li class="is-active"><a>'. $value .'</a></li>';
  }

  echo $tag;
}

$nextStatus = [
  'new' => 'Verifikasi',
  'verified' => 'Proses',
  'processed' => 'Antar',
  'delivered' => 'Selesai'
];
?>

<?php $this->append('css'); ?>
  <?= $this->Html->css('guard/pesanan'); ?>
<?php $this->end(); ?>

<?php $this->append('endScript'); ?>
  <?= $this->Html->script('tableModal'); ?>
<?php $this->end(); ?>

<?= $this->element('guard/navbar'); ?>
<?= $this->element('guard/pesananstatus'); ?>
<section class="section">
  <div class="container">
    <div class="level">
      <div class="level-left">
        <div class="level-item">
          <p class="title">Daftar Pesanan</p>
        </div>
      </div>
      <div class="level-right">
        <div class="level-item">
          <?= $this->Form->create(null, ['type' => 'get']); ?>
          <div class="control field has-addons">
            <div class="control">
              <input id="query" placeholder="Cari Nama Pemesan" class="input" type="text" name="query" value="<?= $query != null ? $query : '' ?>">
            </div>
            <div class="control">
              <input class="button is-primary" type="submit" value="Cari">
            </div>
          </div>
          <?= $this->Form->end(); ?>
        </div>
        <div class="level-item">
          <?php if ($query != null): ?>
          <div class="control">
            <?=$this->Html->link('Reset', ['controller'=>'Pesanan','action'=>'load','prefix'=>'Guard'],['class'=>'button is-danger']); ?>
          </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="table-container">
      <div class="tabs">
        <ul>
          <li><?= tabs('Semua', $query, $status); ?></li>
          <li><?= tabs('New', $query, $status); ?></li>
          <li><?= tabs('Verified', $query, $status); ?></li>
          <li><?= tabs('Processed', $query, $status); ?></li>
          <li><?= tabs('Delivered', $query, $status); ?></li>
        </ul>
      </div>
      <table class="table is-striped is-fullwidth">
        <thead>
          <tr>
            <th class="has-text-right">status</th>
            <th>Nama</th>
            <th>No. HP</th>
            <th class="has-text-centered">Jumlah</th>
            <th class="has-text-right">Tgl Pesan</th>
            <th class="has-text-right">Details</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($pesanan as $data): ?>
          <tr>
            <td class="has-text-right <?= $data->status ?>">
              <?php if($data->status == 'new'): ?>
              <p class="has-text-danger">New</p>
              <?php elseif($data->status == 'verified'): ?>
              <p class="has-text-warning">Verified</p>
              <?php elseif($data->status == 'processed'): ?>
              <p class="has-text-info">Processed</p>
              <?php elseif($data->status == 'delivered'): ?>
              <p class="has-text-success">Delivered</p>
              <?php endif; ?>
            </td>
            <td><?= $data->nama ?></td>
            <td>+62 <?= $data->hp ?></td>
            <td class="has-text-centered"><?= $data->amount ?></td>
            <td class="has-text-right"><?= date('l, j M Y',strtotime($data->order_at)) ?></td>
            <td class="has-text-right">
              <a id="show-pesanan-modal" data-target="<?= $data->id ?>" class="has-text-grey is-size-6 is-family-primary">Tampilkan</a>
            </td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
    <div class="columns is-vcentered">
      <div class="column is-4">
        <p class="has-text-left has-text-centered-touch has-text-dark"><?= $this->Paginator->counter('pages'); ?></p>
      </div>
      <div class="column is-8">
        <nav class="pagination is-centered" role="navigation" aria-label="pagination">
          <?= $this->Paginator->prev('<< Sebelumnya'); ?>
          <?= $this->Paginator->next('Selanjutnya >>'); ?>
          <ul class="pagination-list">
            <?= $this->Paginator->numbers(['first' => 'Awal', 'last' => 'Akhir', 'modulus' => 2]); ?>
          </ul>
        </nav>
      </div>
    </div>
    <?php foreach($pesanan as $data): ?>
    <div class="modal" id="id<?= $data->id ?>">
      <div class="modal-background"></div>
      <div class="modal-card">
        <header class="modal-card-head">
          <p class="modal-card-title">Detail Pesanan <?= $data->nama ?></p>
          <button class="delete" aria-label="close" id="close-modal-button" data-target="<?= $data->id ?>"></button>
        </header>
        <section class="modal-card-body">
          <div class="field is-horizontal">
            <div class="field-label">
              <label class="label" for="nama">Nama</label>
            </div>
            <div class="field-body">
              <div class="field">
                <div class="control is-expanded">
                  <input type="text" class="input" value="<?= $data->nama ?>" readonly>
                </div>
              </div>
            </div>
          </div>
          <div class="field is-horizontal">
            <div class="field-label">
              <label class="label" for="hp">No. HP</label>
            </div>
            <div class="field-body">
              <div class="field">
                <div class="control is-expanded">
                  <input type="text"  class="input" value="+62 <?= $data->hp ?>" readonly>
                </div>
              </div>
            </div>
          </div>
          <div class="field is-horizontal">
            <div class="field-label">
              <label class="label" for="amount">Jumlah</label>
            </div>
            <div class="field-body">
              <div class="field">
                <div class="control is-expanded">
                  <input type="text"  class="input" value="<?= $data->amount ?>" readonly>
                </div>
              </div>
            </div>
          </div>
          <div class="field is-horizontal">
            <div class="field-label">
              <label class="label" for="alamat">Alamat</label>
            </div>
            <div class="field-body">
              <div class="field">
                <div class="control is-expanded">
                  <textarea class="textarea has-fixed-size" value="" readonly><?= $data->alamat ?></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="field is-horizontal">
            <div class="field-label">
              <label class="label" for="note">Catatan</label>
            </div>
            <div class="field-body">
              <div class="field">
                <div class="control is-expanded">
                  <textarea class="textarea has-fixed-size" value="" readonly><?= $data->notes ?></textarea>
                </div>
              </div>
            </div>
          </div>
        </section>
        <footer class="modal-card-foot">
          <nav class="level is-mobile">
            <div class="level-left">
              <div class="level-item">
                <?= $this->Html->link('Hapus', ['controller' => 'Pesanan', 'action' => 'remove', 'prefix' => 'Guard', $data->uid], ['class' => 'button is-danger is-light']); ?>
              </div>
            </div>
            <div class="level-right">
              <div class="level-item">
                <div class="field is-grouped">
                  <div class="control">
                    <p class="button is-text" id="close-modal-button" data-target="<?= $data->id ?>">Tutup</p>
                  </div>
                  <div class="control">
                    <?= $this->Html->link($nextStatus[$data->status], ['controller' => 'Pesanan', 'action' => 'nextProcess', 'prefix' => 'Guard', $data->uid], ['class' => 'button ' . ($data->status != 'delivered' ? 'is-success' : 'is-text'), ($data->status == 'delivered' ? 'disabled' : null)]); ?>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </footer>
      </div>
    </div>
    <?php endforeach; ?>
  </div>
</section>
<?= $this->element('guard/footer'); ?>
