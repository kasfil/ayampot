<?php $this->append('endScript') ?>
<?= $this->Html->script('prizeFormat') ?>
<?= $this->Html->script('element/bulmaflash') ?>
<?php $this->end() ?>

<?= $this->element('guard/navbar'); ?>
<?= $this->Flash->render(); ?>
<section class="section">
  <div class="container">
    <div class="columns is-centered">
      <div class="column is-8">
        <p class="title has-text-dark has-text-left">Update Harga</p>
        <hr>
        <?= $this->Form->create($prizeForm); ?>
        <div class="field is-horizontal">
          <div class="field-label">
            <?= $this->Form->label('kg', 'Harga per Kg', ['class' => 'label is-normal']); ?>
          </div>
          <div class="field-body">
            <div class="field">
              <div class="control is-expanded">
                <?= $this->Form->text('kg', ['class' => 'input', 'id' => 'kgForm', 'oninvalid' => 'this.setCustomValidity(\'Harga tidak boleh kosong.\')', 'autocomplete' => 'off']); ?>
              </div>
              <?php if (isset($formErrors) && isset($formErrors['kg'])): ?>
              <p class="help is-danger"><?= $formErrors['kg']['numeric']; ?></p>
              <?php endif; ?>
            </div>
            <div class="field">
              <div class="control is-expanded">
                <input id="staticKg" class="input is-static" type="text" value="" readonly>
              </div>
            </div>
          </div>
        </div>
        <div class="field is-horizontal">
          <div class="field-label">
            <?= $this->Form->label('ekor', 'Harga per Ekor', ['class' => 'label is-normal']); ?>
          </div>
          <div class="field-body">
            <div class="field">
              <div class="control is-expanded">
                <?= $this->Form->text('ekor', ['class' => 'input', 'id' => 'ekorForm', 'oninvalid' => 'this.setCustomValidity(\'Harga tidak boleh kosong.\')', 'autocomplete' => 'off']); ?>
              </div>
              <?php if (isset($formErrors) && isset($formErrors['ekor'])): ?>
              <p class="help is-danger"><?= $formErrors['ekor']['numeric']; ?></p>
              <?php endif; ?>
            </div>
            <div class="field">
              <div class="control is-expanded">
                <input id="staticEkor" class="input is-static" type="text" value="" readonly>
              </div>
            </div>
          </div>
        </div>
        <div class="field is-horizontal">
          <div class="field-label">
          </div>
          <div class="field-body">
            <div class="field">
              <div class="control">
                <input class="button is-primary" type="submit" value="Perbarui Harga">
              </div>
            </div>
          </div>
        </div>
        <?= $this->Form->end(); ?>
      </div>
    </div>
  </div>
</section>
<?= $this->element('guard/footer'); ?>
