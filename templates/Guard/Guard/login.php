<?= $this->element('guard/navbar'); ?>
<section class="hero is-light is-fullheight-with-navbar">
  <div class="hero-body">
    <div class="container">
      <div class="columns is-centered">
        <div class="column is-4">
          <p class="title has-text-centered has-text-dark">Login</p>
          <?php if(isset($error)): ?>
          <p class="has-text-danger has-text-centered is-size-6 has-text-weight-bold">
            <?= $error ?>
          </p>
          <?php endif; ?>
          <?= $this->Form->create(null, [ 'url' => ['controller' => 'Guard', 'action' => 'login', 'prefix' => 'Guard']]) ?>
          <div class="field">
            <?= $this->Form->label('username', 'Username', ['class' => 'label']); ?>
            <div class="control">
              <?= $this->Form->text('username', ['class' => 'input']); ?>
            </div>
          </div>
          <div class="field">
            <?= $this->Form->label('password', 'Password', ['class' => 'label']); ?>
            <div class="control">
              <?= $this->Form->password('password', ['class' => 'input']); ?>
            </div>
          </div>
          <div class="field">
            <div class="control">
              <input class="button is-primary" type="submit" value="Login">
            </div>
          </div>
          <?= $this->Form->end(); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="hero-foot">
    <?= $this->element('guard/footer'); ?>
  </div>
</section>
