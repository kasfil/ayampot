<?php $this->append('css'); ?>
  <?= $this->Html->css('component/footer'); ?>
<?php $this->end(); ?>

<div class="footer">
  <div class="container">
    <div class="columns is-centered">
      <div class="column is-5">
        <ul class="bullet-icon fa-ul is-size-5-desktop">
          <li class="building">
            <span class="fa-li"><i class="fas fa-building"></i></span>
            Sentral Ayam
          </li>
          <li class="location">
            <span class="fa-li"><i class="fas fa-map-marker-alt"></i></span>
            Jl. Saharjo Raya no. 5 RT 01 RW 05 kel. Manggarai Kec. Tebet Jakarta Selatan
          </li>
          <li class="email">
            <span class="fa-li"><i class="fas fa-envelope-open"></i></span>
            <a class="mailto has-text-dark" href="mailto:prunasimun@gmail.com">prunasimun@gmail.com</a>
          </li>
        </ul>
      </div>
      <div class="column is-3">
        <a class="button is-danger is-fullwidth" href="/pesanan">
          Pesan Sekarang
        </a>
        <br>
        <a class="button is-success is-fullwidth" target="_blank" href="https://wa.me/6285697008485?text=Hi+Sentral+Ayam%21">
          <i class="fab fa-lg fa-whatsapp"></i>&nbsp;
          Chat Kami di WhatsApp
        </a>
      </div>
    </div>
  </div>
</div>
<div class="copyright">
  <div class="columns">
    <div class="column has-text-centered">
      <p class="has-text-dark is-size-6 is-italic">&copy; Copyright Sentral Ayam ~ 2020</p>
    </div>
  </div>
</div>
