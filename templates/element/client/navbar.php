<!-- Adding additional css -->
<?php $this->append('css'); ?>
  <?php echo $this->Html->css('component/navbar'); ?>
<?php $this->end(); ?>

<?php $this->append('endScript'); ?>
  <?php echo $this->Html->script('element/navbar-toggle'); ?>
<?php $this->end(); ?>

<nav class="navbar is-dark" role="navigation">
  <div class="container">
    <div class="navbar-brand">
      <a class="navbar-item" href="/">
        <img
          src="<?php echo $this->Url->image('logo/se-logo-white.png') ?>"
          alt="Ayam Potong Segar"
          />
      </a>
      <a class="navbar-item additional" href=""><span class="icon"><i class="fab fa-lg fa-facebook-square"></i></span></a>
      <a class="navbar-item additional" href=""><span class="icon"><i class="fab fa-lg fa-instagram"></i></span></a>
      <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="DefaultNavbarMenu">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
    </div>
    <div class="navbar-menu" id="DefaultNavbarMenu">
      <div class="navbar-start">
        <a class="navbar-item" href="/pesanan">Pesan Online</a>
        <a class="navbar-item" href="/tentang-kami">Tentang Kami</a>
      </div>
      <div class="navbar-end is-hidden-touch">
        <a class="navbar-item" href="">
          <span class="icon"><i class="fab fa-lg fa-facebook-square"></i></span>
        </a>
        <a class="navbar-item" href="">
          <span class="icon"><i class="fab fa-lg fa-instagram"></i></span>
        </a>
      </div>
    </div>
  </div>
</nav>
