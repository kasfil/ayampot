<?php $this->append('endScript'); ?>
<?= $this->Html->script('element/navbar-toggle.js'); ?>
<?php $this->end(); ?>
<nav class="navbar is-dark" role="navigation">
  <div class="container">
    <div class="navbar-brand">
      <a class="navbar-item" href="/">
        <img
          src="<?= $this->Url->image('logo/se-logo-white.png') ?>"
          alt="Ayam Potong Segar"
          />
      </a>
      <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="dropNavbar">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
    </div>
    <?php if ($this->Identity->isLoggedIn()): ?>
    <div id="dropNavbar" class="navbar-menu">
      <div class="navbar-start">
        <p class="navbar-item">Masuk Sebagai <?= $this->Identity->get('username') ?></p>
      </div>
      <div class="navbar-end">
        <a class="navbar-item" href="/guard/pesanan">
          <span class="icon is-medium">
            <i class="fas fa-child"></i>
          </span>
          Pesanan
        </a>
        <a class="navbar-item" href="/guard/users">
          <span class="icon is-medium">
            <i class="fas fa-users"></i>
          </span>
          Users
        </a>
        <a class="navbar-item" href="/guard/seo">
          <span class="icon is-medium">
            <i class="fas fa-globe-asia"></i>
          </span>
          SEO Data
        </a>
        <a class="navbar-item" href="/guard/prize">
          <span class="icon is-medium">
            <i class="fab fa-bitcoin"></i>
          </span>
          Harga
        </a>
        <a class="navbar-item has-background-danger" href="/guard/logout">
          <span class="icon is-medium">
            <i class="fas fa-sign-out-alt"></i>
          </span>
          Logout
        </a>
      </div>
    </div>
    <?php endif; ?>
  </div>
</nav>
