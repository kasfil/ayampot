<section class="section">
  <div class="container">
    <div class="level add-padding has-background-grey-lighter">
      <div class="level-item has-text-left">
        <div>
          <p class="title">Statistik Pesanan</p>
        </div>
      </div>
      <div class="level-item has-sub">
        <div class="level-item has-text-centered">
          <div>
            <p class="heading">Hari Ini</p>
            <p class="title"><?= $stats['today'] ?></p>
          </div>
        </div>
        <div class="level-item has-text-centered">
          <div>
            <p class="heading">Total</p>
            <p class="title"><?= $stats['total'] ?></p>
          </div>
        </div>
      </div>
    </div>
    <div class="level add-padding has-background-warning">
      <div class="level-item has-sub">
        <div class="level-item has-text-centered">
          <div>
            <p class="heading">New</p>
            <p class="title"><?= $stats['new']; ?></p>
          </div>
        </div>
        <div class="level-item has-text-centered">
          <div>
            <p class="heading">Verified</p>
            <p class="title"><?= $stats['verif']; ?></p>
          </div>
        </div>
      </div>
      <div class="level-item has-sub">
        <div class="level-item has-text-centered">
          <div>
            <p class="heading">Processed</p>
            <p class="title"><?= $stats['procs']; ?></p>
          </div>
        </div>
        <div class="level-item has-text-centered">
          <div>
            <p class="heading">Delivered</p>
            <p class="title"><?= $stats['deliv']; ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
