var showButtons = u('a#show-pesanan-modal');
var closeButtons = u('#close-modal-button');

showButtons.on('click', function(event) {
    event.preventDefault();
    var target = u(this).data('target');
    console.log(target);
    var modal = u('#id'+target);
    modal.addClass('is-active');
});

closeButtons.on('click', function(event) {
    event.preventDefault();
    var targetClose = u(this).data('target');
    var modalClose = u('#id'+targetClose);
    modalClose.removeClass('is-active');
});
