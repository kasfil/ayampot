const Formatter = new Intl.NumberFormat(
    'id-ID',
    {
        'style': 'currency',
        'currency': 'IDR',
        'minimumFractionDigits': 0,
        'maximumFractionDigits': 2
    }
);

let kgFormatTag = u('input#staticKg').first();
let ekorFormatTag = u('input#staticEkor').first();

let kgForm = u('input#kgForm').first();
let ekorForm = u('input#ekorForm').first();

let kgInitVal = parseInt(kgForm.value, 10);
let ekorInitVal = parseInt(ekorForm.value, 10);

kgFormatTag.value = 'Ditampilkan ' + Formatter.format(kgInitVal);
ekorFormatTag.value = 'Ditampilkan ' + Formatter.format(ekorInitVal);

kgForm.addEventListener('keyup', (el) => {
    let value = parseInt(el.target.value, 10);
    if (isNaN(value)) {
        value = 0;
    }
    kgFormatTag.value = 'Ditampilkan ' + Formatter.format(value);
});

ekorForm.addEventListener('keyup', (el) => {
    let value = parseInt(el.target.value, 10);
    if (isNaN(value)) {
        value = 0;
    }
    ekorFormatTag.value = 'Ditampilkan ' + Formatter.format(value);
});
