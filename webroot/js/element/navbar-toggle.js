var navbarBurger = u('.navbar-burger');

navbarBurger.on('click', function(e) {
    e.preventDefault();
    var target = navbarBurger.data('target');
    u('#'+target).toggleClass('is-active');
    navbarBurger.toggleClass('is-active');
});
