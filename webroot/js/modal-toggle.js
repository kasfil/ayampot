var showButtons = u('a#show-modal');
var closeButtons = u('a#close-modal');

showButtons.on('click', function(event) {
    event.preventDefault();
    var target = u(this).data('target');
    var modal = u('#'+target);
    modal.addClass('is-active');
});

closeButtons.on('click', function(event) {
    event.preventDefault();
    var targetClose = u(this).data('target');
    var modalClose = u('#'+targetClose);
    modalClose.removeClass('is-active');
});
