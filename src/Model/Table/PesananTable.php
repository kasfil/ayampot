<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class PesananTable extends Table
{
    public function initialize(array $config): void
    {
        $this->setTable('pesanan');
        $this->setEntityClass('App\Model\Entity\Pesanan');
        $this->addBehavior('Uid');
        $this->addBehavior('Timestamp',[
            'events' => [
                'Model.beforeSave' => [
                    'order_at' => 'new'
                ],
                'Process.Completed' => [
                    'delivery_date' => 'always'
                ]
            ]
        ]);
    }
}
