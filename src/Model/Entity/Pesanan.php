<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Pesanan extends Entity
{
    protected $_visual = ['pesanan_status'];

    protected function _getPesananStatus(): bool
    {
        $verified = (bool) $this->verified;
        $processed = (bool) $this->processed;
        $delivered = (bool) $this->delivered;
        // Get all status of all process
        $status = $verified && $processed && $delivered;

        return $status;
    }
}
