<?php

namespace App\Model\Behavior;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\EventInterface;
use Cake\ORM\Behavior;
use Cake\Utility\Text;

class UidBehavior extends Behavior
{
    protected $_defaultConfig = [
        'field' => 'uid'
    ];

    public function getUid(EntityInterface $entity): void
    {
        $config = $this->getConfig();
        $rawUid = Text::uuid();
        $parsedUid = Text::tokenize($rawUid, '-');
        $uid = implode($parsedUid);
        $entity->set($config['field'], $uid);
    }

    public function beforeSave(EventInterface $event, EntityInterface $entity, ArrayObject $options): void
    {
        $this->getUid($entity);
    }
}
