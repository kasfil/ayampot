<?php
declare(strict_types = 1);

namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

class PesananForm extends Form
{
    protected function _buildSchema(Schema $schema): Schema
    {
        return $schema->addFields([
            'nama' => [
                'type' => 'string'
            ],
            'alamat' => [
                'type' => 'text'
            ],
            'phone' => [
                'type' => 'string'
            ],
            'amount' => [
                'type' => 'float'
            ],
            'amount_type' => [
                'type' => 'string'
            ],
            'notes' => [
                'type' => 'text'
            ]
        ]);
    }

    public function validationDefault(Validator $validator): Validator
    {
        // Define is fields allowed empty
        $validator->allowEmpty('notes')
                  ->allowEmptyString('nama', 'Kolom Nama tidak boleh kosong', false)
                  ->allowEmptyString('alamat', 'Kolom Alamat tidak boleh kosong', false)
                  ->allowEmpty('amount', false, 'Kolom Jumlah Pesanan masih kosong')
                  ->allowEmpty('amount_type', false, 'Tipe jumlah pesanan harus diisi')
              // Defile valid input
              // for 'nama' column
                  ->lengthBetween('nama', [3, 64], 'Minimal 3 karakter Maksimal 64 karakter')
              // for 'alamat' column
                  ->minLength('alamat', 7, 'Minimal 7 karakter')
              // for 'amount' column
                  ->greaterThan('amount', 0.00, 'Pesanan harus di atas 0 Kg atau 0 Ekor')
              // for 'amount_type' column
                  ->inList('amount_type', ['Kg', 'Ekor'], 'Tipe pesanan tidak valid');

        // Return validator
        return $validator;
    }

    protected function _execute(array $data): bool
    {
        $pesananTable = TableRegistry::getTableLocator()->get('Pesanan',[
            'ClassName' => 'App\Model\Table\PesananTable'
        ]);

        $pesanan = $pesananTable->newEmptyEntity();
        $pesanan->nama = $data['nama'];
        $pesanan->alamat = $data['alamat'];
        $pesanan->hp = $data['phone'];
        $pesanan->amount = $data['amount'] . ' ' . $data['amount_type'];
        $pesanan->notes = $data['notes'];
        $pesanan->delivery_date = null;
        // Menyimpan pesanan ke database
        if ($pesananTable->save($pesanan)) {
            return true;
        } else {
            return false;
        }
    }
}
