<?php
declare(strict_types = 1);


namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class SeoForm extends Form
{
    protected function _buildSchema(Schema $schema): Schema
    {
        return $schema->addFields([
            'description' => 'text',
            'keywords' => 'text',
            'robots' => 'string',
            'revisit' => 'integer',
            'canonical' => 'string',
            'analytic_key' => 'string'
        ]);
    }

    public function validationDefault(Validator $validator): Validator
    {
        $validator
            // Rule for description
            ->allowEmptyString('description')
            ->maxLength('description', 150, 'Deskripsi tidak boleh lebih dari 150 karakter')
            // Rule for keywords
            ->allowEmptyString('keywords')
            // Rule for robots
            ->allowEmptyString('robots')
            // Rule for revisit
            ->allowEmptyString('revisit')
            // Rule for canonical
            ->allowEmptyString('canonical')
            // Rule for analytic_key
            ->allowEmptyString('analytic_key');

        return $validator;
    }
}
