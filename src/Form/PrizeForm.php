<?php
declare(strict_types = 1);

namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;
use Lazer\Classes\Database as Lazer;

class PrizeForm extends Form
{
    protected function _buildSchema(Schema $schema): Schema
    {
        return $schema->addFields([
            'ekor' => 'string',
            'kg' => 'string'
        ]);
    }

    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->numeric('ekor', 'Hanya bisa berisi digit', true)
            ->numeric('kg', 'Hanya bisa berisi digit', true);

        return $validator;
    }

    protected function _execute(array $data): bool
    {
        $satuans = ['ekor', 'kg'];
        foreach ($satuans as $satuan) {
            $row = Lazer::table('prize')->where('satuan', '=', $satuan)->find();
            $row->prize = $data[$satuan];
            $row->save();
        }

        return true;
    }
}
