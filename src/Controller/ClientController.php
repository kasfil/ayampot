<?php
declare(strict_types=1);

namespace App\Controller;

use App\Form\PesananForm;
use Lazer\Classes\Database as lazer;

class ClientController extends AppController
{
    protected $tSeo;
    protected $tPrize;

    public function initialize(): void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['order','index','tentangKami']);

        $this->tSeo = lazer::table('seo_data');
        $this->tPrize = lazer::table('prize');
        $this->viewBuilder()->setLayout('client');
        $this->set('meta', $this->_getSeoMeta());
    }

    public function index(): void
    {
        $this->set('title', 'Agen Pemesanan Ayam Potong segar');
        $prize = $this->tPrize->findAll()->asArray('satuan', 'prize');
        $this->set('prize', $prize);

        $this->render('home');
    }

    public function order()
    {
        $pesanan = new PesananForm();
        if ($this->request->is('post')) {
            if ($pesanan->execute($this->request->getData())) {
                $this->Flash->bNotifSuccess('<b>Pesanan Telah Berhasil</b><br />Kami akan memproses pesanan anda.');
                return $this->redirect(
                    [
                    'controller' => 'Client',
                    'action' => 'index'
                    ]
                );
            } else {
                $this->Flash->bNotifError('<b>Kesalahan Pengisian Data Pesanan</b><br />Mohon teliti kembali data pesanan anda.');
            }
        }

        $this->set('pesanan', $pesanan);
        $this->set('title', 'Pesan Online Ayam Potong Segar Diantar ke Rumah');
        $this->render('order');
    }

    public function tentangKami(): void
    {
        $this->set('title', 'Tentang Kami | Sentral Ayam');
        $this->render('tentang_kami');
    }

    protected function _getSeoMeta(): array
    {
        $tSeo = $this->tSeo;
        $seoData = $tSeo->findAll()->asArray('meta', 'value');
        return $seoData;
    }
}
