<?php
declare(strict_types = 1);

namespace App\Controller\Guard;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Database\Expression\QueryExpression;
use Cake\Http\Response;

class PesananController extends AppController
{
    protected $_tPesanan;

    public $paginate = [
        'limit' => 15,
        'order' => [
            'Pesanan.id' => 'desc'
        ]
    ];

    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Paginator');

        $this->viewBuilder()->setLayout('guard');
        $this->_tPesanan = TableRegistry::getTableLocator()->get('Pesanan');
    }

    public function load(): void
    {
        $status = $this->request->getQuery('status');
        $query = $this->request->getQuery('query');

        $stats = $this->_statRecord();

        $listPesanan = $this->_tPesanan->find();

        if ($query != null) {
            $searchQuery = '%' . $query . '%';
            // using query param
            $listPesanan = $listPesanan->where(function (QueryExpression $exp) use ($searchQuery) {
                return $exp->like('nama', $searchQuery);
            });
        }

        switch ($status) {
            case 'new':
                $listPesanan->where(['status' => 'new']);
                break;
            case 'verified':
                $listPesanan->where(['status' => 'verified']);
                break;
            case 'processed':
                $listPesanan->where(['status' => 'processed']);
                break;
            case 'delivered':
                $listPesanan->where(['status' => 'delivered']);
                break;
        }

        $pesanan = $this->paginate($listPesanan);

        $this->set('stats', $stats);
        $this->set('query', $query == null ? null : $query);
        $this->set('pesanan', $pesanan);
        $this->set('status', $status == null ? 'semua' : $status);

        $this->render('load');
    }

    protected function _statRecord(): array
    {
        $tPesanan = $this->_tPesanan;
        // Stat Pesanan Today
        $today = date('Y-m-d');
        $recordToday = $tPesanan->find()->where(function (QueryExpression $exp) use ($today) {
            return $exp->like('order_at', "%".$today."%");
        })->count();
        // Stat Pesanan Total
        $recordTotal = $tPesanan->find()->count();
        // Stat Pesanan unverified
        $recordNew = $tPesanan->find()->where(['status' => 'new'])->count();
        // Stat Pesanan Verified
        $recordVer = $tPesanan->find()->where(['status' => 'verified'])->count();
        // Stat Pesanan processed
        $recordProc = $tPesanan->find()->where(['status' => 'processed'])->count();
        // Stat Pesanan Delivered
        $recordDelv = $tPesanan->find()->where(['status' => 'delivered'])->count();

        // Merge into variable
        $stats = [
            'total' => $recordTotal,
            'today' => $recordToday,
            'new' => $recordNew,
            'verif' => $recordVer,
            'procs' => $recordProc,
            'deliv' => $recordDelv
        ];

        return $stats;
    }

    public function nextProcess(string $uid): Response
    {
        $status = ['verified', 'processed', 'delivered'];
        $tPesanan = $this->_tPesanan;

        $pesanan = $tPesanan->find()->where(['uid' => $uid])->first();

        if ($pesanan != null) {
            switch ($pesanan->status) {
                case 'processed':
                    $pesanan->status = $status[2];
                    $pesanan->delivery_date = date("Y-m-d H:i:s");
                    break;
                case 'verified':
                    $pesanan->status = $status[1];
                    break;
                case 'new':
                    $pesanan->status = $status[0];
                    break;
                default:
                    break;
            }

            $tPesanan->save($pesanan);
        }

        return $this->redirect([
            'controller' => 'Pesanan',
            'action' => 'load'
        ]);
    }

    public function remove(string $uid): Response
    {
        $tPesanan = $this->_tPesanan;
        $pesanan = $tPesanan->find()->where(['uid' => $uid])->first();

        if ($pesanan != null) {
            $tPesanan->delete($pesanan);
        }

        return $this->redirect([
            'controller' => 'Pesanan',
            'action' => 'load'
        ]);
    }
}
