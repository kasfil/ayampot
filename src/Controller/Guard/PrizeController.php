<?php
declare(strict_types = 1);

namespace App\Controller\Guard;

use App\Controller\AppController;
use App\Form\PrizeForm;
use Lazer\Classes\Database as Lazer;
use Lazer\Classes\Helpers\Validate as LazerValidate;
use Lazer\Classes\LazerException;

class PrizeController extends AppController
{
    protected $tPrizes;

    public function initialize(): void
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('guard');

        try {
            LazerValidate::table('prize')->exists();
        } catch(LazerException $e) {
            $this->createPrizeDb();
        }

        $this->tPrizes = Lazer::table('prize');
    }

    public function index(): void
    {
        $prizeForm = new PrizeForm();
        // POST request
        if ($this->request->is('post')) {
            if ($prizeForm->execute($this->request->getData())) {
                $this->Flash->bNotifSuccess('Update harga berhasil');
            } else {
                $this->set('formErrors', $prizeForm->getErrors());
            }
        }

        $prize = $this->tPrizes->findAll()->asArray('satuan', 'prize');
        $prizeForm->setData([
            'kg' => $prize['kg'],
            'ekor' => $prize['ekor']
        ]);
        $this->set('prizeForm', $prizeForm);
    }

    protected function createPrizeDb(): void
    {
        Lazer::create('prize', [
            'id' => 'integer',
            'satuan' => 'string',
            'prize' => 'string'
        ]);

        $row = Lazer::table('prize');

        $rowsInitData = [
            'ekor' => '23000',
            'kg' => '23000'
        ];

        foreach ($rowsInitData as $key => $value) {
            $row->satuan = $key;
            $row->prize = $value;
            $row->save();
        }
    }
}
