<?php

namespace App\Controller\Guard;

use App\Controller\AppController;
use App\Form\SeoForm;
use Lazer\Classes\Database as lazer;
use Lazer\Classes\Helpers\Validate as lazerValidate;
use Lazer\Classes\LazerException;

class SeoController extends AppController
{
    protected $tSeo;

    public function initialize(): void
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('guard');

        try {
            lazerValidate::table('seo_data')->exists();
        } catch (LazerException $e) {
            $this->firstCreate();
        }

        $this->tSeo = lazer::table('seo_data');
    }

    public function load(): void
    {
        $seoForm = new SeoForm();
        $tSeo = $this->tSeo;

        // if request is post means update value tag
        if ($this->request->is('post')) {
            // Get user submit data
            $submitedData = $this->request->getData();
            // Validate user input
            if ($seoForm->validate($submitedData)) {
                // Update each meta and value
                foreach ($submitedData as $meta => $value) {
                    $row = $tSeo->where('meta', '=', $meta)->find();
                    // Ensure meta is available in database
                    if ($row != null){
                        $row->value = $value;
                        $row->save();
                    }
                }
            } else {
                $this->set('formErrors', $seoForm->getErrors());
            }
        }

        if ($this->request->is('get')) {
            $seoData = $tSeo->findAll()->asArray('meta', 'value');
            $seoForm->setData([
                'description' => $seoData['description'],
                'keywords' => $seoData['keywords'],
                'robots' => $seoData['robots'],
                'revisit' => $seoData['revisit'],
                'canonical' => $seoData['canonical'],
                'analytic_key' => $seoData['analytic_key'],
            ]);
        }

        $this->set('seoForm', $seoForm);

        $this->render('load');
    }

    private function firstCreate():void
    {
        lazer::create('seo_data', [
            'id' => 'integer',
            'meta' => 'string',
            'value' => 'string'
        ]);

        $row = lazer::table('seo_data');

        $rowsData = [
            'description' => 'Agen pemesanan ayam potong segar terpercaya, melayani pesanan online, antar sampai alamat tujuan',
            'keywords' => 'agen ayam potong, pesan ayam potong, pesan online ayam potong, delivery ayam potong, ayam potong delivery, ayam potong segar, ayam potong online, ayam potong, ayam potong jakarta',
            'robots' => 'index, follow',
            'revisit' => '3',
            'canonical' => 'sentralayam.com',
            'analytic_key' => ''
        ];

        foreach ($rowsData as $key => $value) {
            $row->meta = $key;
            $row->value = $value;
            $row->save();
        }
    }
}
